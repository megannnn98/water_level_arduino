#include <EEPROM.h>

//This function will write a 2 byte integer to the eeprom at the specified address and address + 1
void EEPROMWriteInt(int p_address, int p_value)
{
  byte lowByte = ((p_value >> 0) & 0xFF);
  byte highByte = ((p_value >> 8) & 0xFF);
  
  EEPROM.write(p_address, lowByte);
  EEPROM.write(p_address + 1, highByte);
}

//This function will read a 2 byte integer from the eeprom at the specified address and address + 1
unsigned int EEPROMReadInt(int p_address)
{
  byte lowByte = EEPROM.read(p_address);
  byte highByte = EEPROM.read(p_address + 1);
  
  return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}




#define SEC   (1000)
#define MIN   (SEC*60)
#define HOUR  (MIN*60)

#define INTERVAL     (160*SEC)
#define ANALOG_PIN   3
#define VCC_PIN      8
#define MAX_ADDR_EEPROM 511


unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
int address = 0;
int tmp = 0;           // variable to store the value read

void setup()
{
  pinMode(VCC_PIN, OUTPUT);
  Serial.begin(9600);          //  setup serial
}

void loop()
{
  processWriring();
  processSerial();
}




void processWriring()
{
  currentMillis = millis();
 
  if(currentMillis - previousMillis > INTERVAL) {
    previousMillis = currentMillis;   

    if (address < MAX_ADDR_EEPROM)
    {    
      digitalWrite(VCC_PIN, HIGH);
      {
        delay(1);      
        EEPROMWriteInt(address, analogRead(ANALOG_PIN));
      }
      digitalWrite(VCC_PIN, LOW);
  
      address += 2;
    }
  }
}
void processSerial()
{
  if (Serial.available() > 0) 
  { 
    do 
    {
      Serial.read();
    } while(Serial.available() > 0); 
    
    
    Serial.print("start");    
    Serial.println(address);
    
    for (int i = 0; i < address; i += 2)
    {
      tmp = EEPROMReadInt(i);
      Serial.print(tmp);
      Serial.println("");
    }
  
    Serial.println("stop");

    delay(SEC);
  }
}











